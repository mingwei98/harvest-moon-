import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo: {
      user: '',
      token: '',
    }
  },
  mutations: {
    setUser(state, payload) {
      state.userInfo = payload;
    },
    clearUser(state) {
      state.userInfo = {
        user: '',
        token: '',
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
