//请求方法封装
import base from './base'
//node转换文本模块
const qs = require('querystring')
import axios,{get,post} from '../utils/request';
const api = {
    // 登录接口
    getLogin(params) {//params={username:'',password:''}
        return get(base.host2+base.user, params);
    },
    //玩具数据接口
    getUser(params){//token
        return get(base.host2+base.userInfo+'?token='+params.token);
    },
    //修改密码接口
    changPass(params){
        return get(base.host2+base.changPass,params);
    },
    //上传图片接口
    upload(params){
        return post(base.host2+base.upload,params);
    }
}

export default api;