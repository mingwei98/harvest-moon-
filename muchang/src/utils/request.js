import axios from "axios"
//创建的axios实例
const instance = axios.create({
    timeout: 5000
})
//再次封装get请求
export function get(url, params) {
    return new Promise((resolve, reject) => {
        instance.get(url, {
            params
        }).then(res => {
            resolve(res.data);
        }).catch(err => {
            reject(err.data);
        })
    })
}

//封装的post
export function post(url, params) {
    return new Promise((resolve, reject) => {
        instance.post(url, params).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.data)
        })
    })
}

export default instance