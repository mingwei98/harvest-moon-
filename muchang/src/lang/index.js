import Vue from 'vue';
import Element from 'element-ui';
import VueI18n from 'vue-i18n';
import enLocale from 'element-ui/lib/locale/lang/en';
import zhLocale from 'element-ui/lib/locale/lang/zh-CN';
//导入自己的语言包

Vue.use(VueI18n)
//准备语言
const messages = {
    //1. 中文
    zh: {
        user: {
            gohome:"返回主页",
            home:"主页",
            userMid:"个人中心",
            userInfo: '个人信息',
            userCard: '账户',
            userName: '昵称',
            userID: 'ID',
            renZheng: "实名认证",
            Money: '游戏币',
            Meta: 'Meta',
            safety: "账号安全",
            authentication: '认证信息',
            password: '原密码',
            passwordNew: '新密码',
            wallet: '钱包',
            exchange: '金币兑换',
            renZ:"已认证",
        },
        home: {
            login:"登录",
            hi:"欢迎",
            book:"白皮书",
            kaifa:"画面均为开发中内容",
            tit:"在元宇宙社区中体验幸福的农场生活",
            jieshao1: 'Meta结合园元社区首次推出元宇宙农场玩法，您可以在元宇宙的世界里开辟一块属于自己的农场，在这里种植收获，出售自己的作物获得Meta币。随着农场建造的不断成熟可以开辟属于自己的牧场、林场、矿洞和海洋渔场，自由选择你喜欢的发展方向和私有社交圈，打造一个精美绝伦的艺术家园。',
            jieshao2: '当元宇宙世界来临时，您开辟的农场将会在元宇宙中占据一块完全属于您自己的独有空间；你还可以访问好友的农场参与好友农场空间的建造与发展，共同打造一个美轮美奂的元社区。农场经营获得的Meta币和成就值可以在元宇宙中购买更多的土地和特定装扮，在元宇宙的世界中建造属于你的房子和打造属于你个人的世外桃源，在里面结识更多的朋友，拥有不一样的人生！',
            wanfa1:"在您的农场中开辟土地，种上您喜欢的种植物，为其浇水、除虫、除草，呵护其健康成长，体会收获的喜悦",
            wanfa2:'开垦新的土地，以种植更多的作物；升级已开垦的土地，让土地更有价值，能够种植更高级的作物，获得更多的产量',
            wanfa3:"您可以随时访问你的好友农场，为他的作物浇水、除虫、除草，助其健康成长；也可以偷偷的收割好友成熟的作物，给好友一份“惊喜”",
            wanfa4:"在您农场的旁边，有一块海域等着您降临；您可以拿上鱼竿，喊上好友，一起享受钓鱼的乐趣，体验海滩度假的风情",
            wanfa5:"您可以在和其他玩家自由交易您在游戏中获得的一切财富",
            wanfa6:"在游戏中赚取金币，获得魅力值和财富值，有机会兑换限时发行的限量装扮皮肤，向元宇宙的玩家展示你的藏品",
            jiewei1:"随着您的元宇宙社区的建设模块不断扩大，您在元宇宙世界地图上展示的区域会随之增大，让元宇宙所有玩家都能够看到您的个人空间，获得巨量影响力，让您在未来的元宇宙世界里成为最耀眼的星辰",
            jiewei2:"未来我们还将推出：林场种植，栽培属于你的森林；矿场开发，获取源源不断的矿石产出；航海贸易，与元宇宙其他玩家经济贸易，互通有无；宠物系统，能够守护你的家园，跟随你探索元宇宙世界",
            jiewei3:"本社区由meta公司核心研发团队制作，率先提出元社区个人空间概念，旨在为玩家打造一个未来元宇宙真正属于自己的魅力家园",
        },
        //导入element-ui里面的国际化语法
        // ...enLocale,
    },
    //2. 英文
    en: {
        user: {
            gohome:"goHome",
            home:"Home",
            userMid:"Personal center",
            userInfo: 'UserInfo',
            userCard: 'userNum',
            userName: 'userName',
            userID: 'ID',
            renZheng: "authentication",
            Money: 'Money',
            Meta: 'Meta',
            safety: 'safety',
            authentication: "authentication",
            password: 'password',
            passwordNew: 'passwordNews',
            wallet: 'wallet',
            exchange: 'exchange',
            renZ:"certified",
        },
        home: {
            login:"login",
            hi:"welcome",
            book:"The white paper",
            kaifa:"All screens are under development",
            tit:"Experience the bliss of farm life in the meta-cosmic community",
            jieshao1: 'Meta launched the Meta Universe farm gameplay for the first time in combination with yuan Yuan community. You can open up a farm of your own in the Meta universe, grow and harvest here, and sell your own crops for Meta coins.  As the farm is built, you can open up your own pasture, forest farm, mine hole and ocean fishing ground. You can freely choose your favorite development direction and private social circle to create an exquisite art home.',
            jieshao2: "When the metasverse comes, the farm you create will occupy a space entirely your own in the metasverse. You can also visit your friend's farm and participate in the construction and development of your friend's farm space to create a beautiful yuan community together.  The Meta coins and achievements earned by farming can buy more land and specific costumes in the metasomes, build your own house and create your own personal paradise in the metasomes, make more friends and have a different life! ",
            wanfa1:"Open up land in your farm, plant your favorite plants, water, insect control, weeding, care for their healthy growth, experience the joy of harvest",
            wanfa2:'Clearing new land to grow more crops; Upgrade cultivated land to make it more valuable for higher quality crops and higher yields',
            wanfa3:"You can visit your friend's farm at any time to water, de-worm and weed his crops to help them grow healthily. You can also secretly harvest your friends' ripe crops and give them a 'surprise'.",
            wanfa4:"Next to your farm, there is a sea waiting for you; You can grab a fishing rod, call a friend, enjoy the fun of fishing together, experience the style of beach vacation",
            wanfa5:"You can freely trade any wealth you earn in the game with other players",
            wanfa6:"Earn coins in the game, gain charisma and wealth, and have a chance to redeem a limited number of dressed-up skins for a limited time, showing your collection to players in the metasomes",
            jiewei1:"As the building blocks of your meta-universe community continue to expand, the area displayed on the map of the meta-universe will increase accordingly, allowing all players in the meta-universe to see your personal space, gaining enormous influence and making you the most dazzling star in the future meta-universe",
            jiewei2:"In the future, we will also launch: forest planting, cultivation belongs to your forest; Mine development, access to a steady stream of ore output; Maritime trade, trade with other players in the meta-universe, and exchange goods and needs; Pet system, can guard your home, follow you to explore the metaverse world",
            jiewei3:"This community is made by the core RESEARCH and development team of Meta Company. It is the first to propose the concept of meta community personal space, aiming to create a future meta-universe for players to truly belong to their charming home",
        }
        // ...zhLocale
    },
    // 日语
    jp: {
        user: {
            gohome:"ホームに戻る",
            home:"ホーム・ページ",
            userMid:"パーソナルセンター",
            userInfo: '個人情報',
            userCard: 'アカウント',
            userName: 'ニックネーム',
            userID: 'ID',
            renZheng: "実名認証",
            Money: 'メダル枚',
            Meta: 'Meta',
            safety: "アカウントセキュリティ",
            authentication: '認証情報',
            password: '元のパスワード',
            passwordNew: '新しいパスワード',
            wallet: '財布',
            exchange: '金貨の両替',
            renZ:"認証済み",
        },
        home: {
            login:"ログイン",
            hi:"ようこそ",
            book:"白書",
            kaifa:"画面はすべて開発中のもの",
            tit:"メタ宇宙コミュニティで幸せな農場生活を体験する",
            jieshao1:"「メタはメタコミュニティと組み合わせて、メタバースファームのゲームプレイを初めて開始しました。メタバースの世界で自分の農場を開き、ここで作物を育て、作物を売ってメタコインを手に入れることができます。農場建設の継続的な成熟により、あなたはあなた自身の牧草地、森林農場、鉱山、海洋漁場を開き、あなたの好きな開発方向と私的な社交界を自由に選び、そして絶妙な芸術の家を作ることができます。",
            jieshao2:"メタバースの世界が来ると、あなたが開いた農場は完全にあなたのものであるメタバースのユニークなスペースを占有します。また、友人の農場を訪れて友人の農場スペースの建設と開発に参加し、共同で美しいメタコミュニティを作成することができます。農場運営から得られるメタコインとアチーブメントポイントは、メタバースで多くの土地と特定の装飾品を購入し、メタバースの世界であなた自身の家とあなた自身の楽園を建て、その中で多くの友達に会って、特異な人生を持つことができます。",
            wanfa1:"あなたの農場に土地を開き、あなたの好きな植物を植え、水をやって、虫除けして、除草して、植物の健康的な成長を世話し、そして収穫の喜びを体験することができます。",
            wanfa2:'新しい土地を耕してたくさんの作物を育て、すでに耕作された土地をアップグレードして、土地の価値をさせて、より高級な作物を育て、より多くの収穫を得ることができます。',
            wanfa3:"いつでも友達の農場を訪れて、彼の作物に水をやったり、虫除けしたり、除草したりして、健康に育てることができます。また、友達の成熟した作物を密かに収穫して、友達に「サプライズ」を与えることもできます。",
            wanfa4:"あなたの農場の隣にはあなたを待っている海域があります。あなたは釣り棒を持って、あなたの友人を誘って、一緒に釣りを楽しんで、そしてビーチ休暇を体験することができます。",
            wanfa5:"ゲームで得たすべての財産を他のプレーヤーと自由に交換できます。",
            wanfa6:"ゲームでコインを獲得し、魅力点と財産点を獲得し、期間限定の装飾品のスキンと交換することとメタバースのプレーヤーにコレクションを展示する機会があります。",
            jiewei1:"メタバースのコミュニティの建設モジュールが拡大することに従って、メタバース世界に展示する領域も拡大し、メタバースのすべてのプレイヤにあなたの個人的なスペースを見せて、巨大な影響力を獲得し、未来のメタバース世界であなたが最も明るい星になることができます。",
            jiewei2:"将来には、植林、自分の森を耕し、鉱山開発、絶え間ない鉱石生産量の獲得、航海貿易、メタバースの他のプレーヤーとの経済貿易、相互のコミュニケーションができて、ペットシステムがあなたの家を守ることができて、あなたと共にメタバースの世界を探索することができます。",
            jiewei3:"このコミュニティは、メタ会社の中核の研究開発チームによって作成され、先立ってメタコミュニティのパーソナルスペースの概念を提案します。プレーヤーに未来のメタバースで魅力的な家が作られることを目指しています。",
        },
    },
    // 韩语
    kr: {
        user: {
            gohome:"반회 주하",
            home:"(주)",
            userMid:"1인 중심",
            userInfo: '신인 신식',
            userCard: '저승',
            userName: '사칭',
            userID: 'ID',
            renZheng: "실명 인증",
            Money: '유희폐',
            Meta: 'Meta',
            safety: "저스호 안전",
            authentication: '인증 신숨',
            password: '원밀수',
            passwordNew: '신밀수',
            wallet: '동전',
            exchange: '금폐환표',
            renZ:"인증됨",
        },
        home: {
            login:"로그인",
            hi:"환영 합니다",
            book:"백서",
            kaifa:"화면은 모두 개발 중인 내용이다",
            tit:"모우주공동체에서 행복한 농장생활을 체험한다",
            jieshao1: '메타 결합 주원사 구 수차 추출원 우주농장 완법, 가이재원 우주적 세계리 개벽 속우 자기적 농장, 재구리 종식 수획, 출렁 자기적 작물 획득 meta 폐.수착농장 건조적 부단성숙 가이개벽속 우자기적 목장, 림장, 광동 와해양어장, 자유선택 와희환적 머리전 방향 와사유사교권, 타조일리쿠세이미절린적 예술가원.',
            jieshao2: '메타 우주가 도래하면, 당신이 만든 농장은 메타 우주의 한 부분을 독점적으로 차지할 것입니다.또한 친구 농장을 방문하여 친구 농장 공간의 건설과 개발에 참여하여 함께 아름답고 절묘한 공동체를 만들 수 있다.농장 경영으로 얻은 메타페와 성취치로는 메타우주에서 더 많은 토지와 특정 의상을 구매할 수 있으며, 메타우주에서 나만의 집과 나만의 파라다이스를 만들 수 있으며, 그 안에서 더 많은 친구들을 사귀고 다른 인생을 살 수 있다!',
            wanfa1:"당신의 농장에 밭을 일구고 당신이 좋아하는 식물을 심고 물을 주고, 벌레를 제거하고, 잡초를 제거하고, 건강하게 가꾸고, 수확의 기쁨을 느끼십시오",
            wanfa2:'새로운 땅을 개간하여 더 많은 작물을 심으며이미 개간한 토지를 개량하여보다 가치있는 토지로 만들고보다 고급작물을 재배하여보다 많은 수확고를 거두게 한다',
            wanfa3:"당신은 언제든지 친구의 농장에 방문하여 그의 작물에 물을 주고, 벌레를 제거하고, 잡초를 제거하여 친구의 건강한 성장을 도울 수 있습니다.또한 몰래 친구들의 익은 농작물을 수확하여 친구들에게 놀라움을 안겨줄수도 있다.",
            wanfa4:"당신의 농장 옆에는 바다가 당신을 기다리고 있습니다.당신은 낚싯대를 들고 친구를 불러 함께 낚시의 즐거움을 즐길 수 있고 해변 휴가의 풍치 또한 체험할 수 있다",
            wanfa5:"당신은 게임에서 얻은 모든 재산을 다른 플레이어와 자유롭게 거래할 수 있습니다",
            wanfa6:"게임 내 금화를 획득하면 매력치와 재부치를 획득할 수 있으며, 한시적으로 발행되는 한정 분장 스킨을 교환하여 원우주의 플레이어에게 자신의 소장품을 보여줄 수 있다",
            jiewei1:"당신의 메타코스모 커뮤니티의 건설 모듈이 계속 확대됨에 따라, 당신은 메타코스모 세계 지도상의 영역을 표시할 수 있으며, 모든 플레이어는 당신의 개인 공간을 볼 수 있고, 엄청난 영향력을 얻을 수 있으며, 미래의 메타코스모 세계에서 가장 빛나는 별이 될 수 있습니다",
            jiewei2:"앞으로 우리는 삼림 농장, 당신의 삼림을 가꾸는 것,광산을 개발하여 끊임없는 광석산출을 얻으며항해무역, 기타 플레이어와 경제무역을 하여 유무상통함.당신의 집을 지켜주고 메타우주를 탐험해 줄 애완동물 시스템",
            jiewei3:"본 커뮤니티는 meta 사의 핵심 연구 개발팀에 의해 제작되었으며, 가장 먼저 메타커뮤니티 개인 공간 개념을 제시하였으며, 플레이어들에게 미래의 메타우주가 진정으로 자기에게 속하는 매력적인 집을 만드는 것을 목표로 한다",
        }
    },
    // 繁体
    zht: {
        user: {
            gohome:"返回主頁",
            home:"主頁",
            userMid:"個人中心",
            userInfo: '個人信息',
            userCard: '賬戶',
            userName: '昵稱',
            userID: 'ID',
            renZheng: "實名認證",
            Money: '遊戲幣',
            Meta: 'Meta',
            safety: "賬號安全",
            authentication: '認證信息',
            password: '原密碼',
            passwordNew: '新密碼',
            wallet: '錢包',
            exchange: '金幣兌換',
            renZ:"已認證",
        },
        home: {
            login:"登錄",
            hi:"歡迎",
            book:"白皮書",
            kaifa:"畫面均為開發中內容",
            tit:"在元宇宙社區中體驗幸福的農場生活",
            jieshao1: 'Meta結合園元社區首次推出元宇宙農場玩法，您可以在元宇宙的世界裏開辟一塊屬於自己的農場，在這裏種植收獲，出售自己的作物獲得Meta幣。隨著農場建造的不斷成熟可以開辟屬於自己的牧場、林場、礦洞和海洋漁場，自由選擇你喜歡的發展方向和私有社交圈，打造一個精美絕倫的藝術家園。',
            jieshao2: '當元宇宙世界來臨時，您開辟的農場將會在元宇宙中占據一塊完全屬於您自己的獨有空間；你還可以訪問好友的農場參與好友農場空間的建造與發展，共同打造一個美輪美奐的元社區。農場經營獲得的Meta幣和成就值可以在元宇宙中購買更多的土地和特定裝扮，在元宇宙的世界中建造屬於你的房子和打造屬於你個人的世外桃源，在裏面結識更多的朋友，擁有不一樣的人生！',
            wanfa1:"在您的農場中開辟土地，種上您喜歡的種植物，為其澆水、除蟲、除草，呵護其健康成長，體會收獲的喜悅",
            wanfa2:'開墾新的土地，以種植更多的作物；升級已開墾的土地，讓土地更有價值，能夠種植更高級的作物，獲得更多的產量',
            wanfa3:"您可以隨時訪問你的好友農場，為他的作物澆水、除蟲、除草，助其健康成長；也可以偷偷的收割好友成熟的作物，給好友一份「驚喜」",
            wanfa4:"在您農場的旁邊，有一塊海域等著您降臨；您可以拿上魚竿，喊上好友，一起享受釣魚的樂趣，體驗海灘度假的風情",
            wanfa5:"您可以在和其他玩家自由交易您在遊戲中獲得的一切財富",
            wanfa6:"在遊戲中賺取金幣，獲得魅力值和財富值，有機會兌換限時發行的限量裝扮皮膚，向元宇宙的玩家展示你的藏品",
            jiewei1:"隨著您的元宇宙社區的建設模塊不斷擴大，您在元宇宙世界地圖上展示的區域會隨之增大，讓元宇宙所有玩家都能夠看到您的個人空間，獲得巨量影響力，讓您在未來的元宇宙世界裏成為最耀眼的星辰",
            jiewei2:"未來我們還將推出：林場種植，栽培屬於你的森林；礦場開發，獲取源源不斷的礦石產出；航海貿易，與元宇宙其他玩家經濟貿易，互通有無；寵物系統，能夠守護你的家園，跟隨你探索元宇宙世界",
            jiewei3:"本社區由meta公司核心研發團隊製作，率先提出元社區個人空間概念，旨在為玩家打造一個未來元宇宙真正屬於自己的魅力家園",
        }
    }

}

//2. 通过选项创建 VueI18n 实例
const i18n = new VueI18n({
    locale: 'zh',//选中的语言
    messages,//语言环境
})

//兼容写法
// Vue.use(Element, {
//     i18n: (key, value) => i18n.t(key, value)
// })

//3. 导出i18n 
export default i18n